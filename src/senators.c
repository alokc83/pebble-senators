#include "pebble_os.h"
#include "pebble_app.h"
#include "pebble_fonts.h"


#define MY_UUID { 0xAA, 0xEE, 0xC7, 0x5F, 0x74, 0x8F, 0x43, 0xA9, 0x90, 0x04, 0x3A, 0xB2, 0x83, 0x07, 0x11, 0x4A }
PBL_APP_INFO(MY_UUID,
             "Senators", "Scott Aitchison",
             1, 0, /* App version */
             DEFAULT_MENU_ICON,
             APP_INFO_WATCH_FACE);

Window window;
TextLayer text_time_layer;
GFont font_time;
BmpContainer background_image;

void update_display(PblTm *tick_time) {
	static char time_text[] = "00:00";
	char *time_format;

	if (clock_is_24h_style()) {
		time_format = "%R";
	} 
	else {
		time_format = "%I:%M";
	}

	string_format_time(time_text, sizeof(time_text), time_format, tick_time);
	if (!clock_is_24h_style() && (time_text[0] == '0')) {
		memmove(time_text, &time_text[1], sizeof(time_text) - 1);
	}

	text_layer_set_text(&text_time_layer, time_text);
}

void handle_init(AppContextRef ctx) {
	(void)ctx;

	window_init(&window, "Window Name");
	window_stack_push(&window, false /* Animated */);
	window_set_background_color(&window, GColorBlack);
	resource_init_current_app(&APP_RESOURCES);

	/* Senators image */
	bmp_init_container(RESOURCE_ID_IMAGE_BACKGROUND, &background_image);
	bitmap_layer_set_background_color(&background_image.layer, GColorBlack);
	layer_set_frame(&background_image.layer.layer, GRect(0, 0, 144, 168));
	layer_add_child(&window.layer, &background_image.layer.layer);

	/* Time layer */
	text_layer_init(&text_time_layer, window.layer.frame);
	text_layer_set_text_color(&text_time_layer, GColorWhite);
	text_layer_set_background_color(&text_time_layer, GColorClear);
	layer_set_frame(&text_time_layer.layer, GRect(80, 10, 144-80, 28));
	text_layer_set_text_alignment(&text_time_layer, GTextAlignmentLeft);
	font_time = fonts_get_system_font(FONT_KEY_GOTHIC_28_BOLD);
	text_layer_set_font(&text_time_layer, font_time);
	layer_add_child(&window.layer, &text_time_layer.layer);

	PblTm tick_time;
	get_time(&tick_time);
	update_display(&tick_time);
}


void handle_deinit(AppContextRef ctx) {
	(void)ctx;

	bmp_deinit_container(&background_image);
}

void handle_minute_tick(AppContextRef ctx, PebbleTickEvent *t) {
	(void)ctx;

	update_display(t->tick_time);
}

void pbl_main(void *params) {
	PebbleAppHandlers handlers = {
		.init_handler = &handle_init,
		.deinit_handler = &handle_deinit,
		.tick_info = {
			.tick_handler = &handle_minute_tick,
			.tick_units = MINUTE_UNIT
		}
	};
	app_event_loop(params, &handlers);
}
